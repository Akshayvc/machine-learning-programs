"""
3.	Write a program to demonstrate the working of the decision tree based ID3 
algorithm. Use an appropriate data set for building the decision tree and apply 
this knowledge to classify a new sample. 

 -----   Iterative Dichotomiser -----

Steps:
    Phase 1: Entropy Calculation
    Phase 2: Attribute Selection
    Phase 3: Split the data set
    Phase 4: Build Decision Tree
"""


import pandas as pd
import math

def calc_entropy(data):
    
    lendata = len(data)
    labels = {} # For reading the labels in the data file
    for rec in data:
        label = rec[-1]
        if label not in labels.keys():
            labels[label] = 0       
            labels[label] += 1
    entropy = 0.0
        
    for key in labels:
        prob = float(labels[key])/lendata
        entropy -= prob * math.log(prob,2)                
    return entropy

def Attr_Selec(data):
    features = len(data[0]) - 1
    base_entropy = calc_entropy(data)
    max_infogain = 0.0
    bestattr = -1
   
    for i in range(features):
        attrlist = [rec[i] for rec in data]
        unqvalues = set(attrlist)
        newentr = 0.0
        
        for value in unqvalues:
            newdata = dataset_split(data, i, value)
            prob = len(newdata)/float(len(data))
            newentr += prob * calc_entropy(newdata)
            infogain = base_entropy - newentr
            if (infogain > max_infogain):
                max_infogain = infogain
                bestattr = i    
    return bestattr

def dataset_split(data,i,val):
    newData = []
    for rec in data:
        if rec[i] == val:
            reducedSet = list(rec[:i])
            reducedSet.extend(rec[i+1:])
            newData.append(reducedSet)
            #print(newData)            
    return newData

def decision_tree(data, labels):
    
    classList = [rec[-1] for rec in data]
    
    if classList.count(classList[0]) == len(classList):
        return classList[0]
    
    maxgainnode = Attr_Selec(data)
    treeLabel = labels[maxgainnode] 
    theTree = {treeLabel:{}}
    del(labels[maxgainnode])
    nodeValues = [rec[maxgainnode] for rec in data]
    uniqueVals = set(nodeValues)
    for value in uniqueVals:
        subLabels = labels[:]
        theTree[treeLabel][value] = decision_tree(dataset_split(data, 
               maxgainnode, value),subLabels) 
    return theTree


data = pd.read_csv('id_data.csv', sep= ',', header= None)
data = data.values
labels = ['Outlook', 'Temp', 'Humid', 'Wind', 'Play']

print(decision_tree(data, labels))
