"""
8.	Apply EM algorithm to cluster a set of data stored in a .CSV file. Use the 
same data set for clustering using k-Means algorithm. Compare the results of 
these two algorithms and comment on the quality of clustering. You can add 
Java/Python ML library classes/API in the program.

ACK and Reference: Python Data Science Handbook by Jake VanderPlas, O'Reilly Publications

Yerriswamy T.
Department of Computer Science abd Engineering, K L E Institute of Technology
Hubballi
"""
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()  # for plot styling
import numpy as np

from sklearn.datasets.samples_generator import make_blobs
X, y_true = make_blobs(n_samples=300, centers=4,
                       cluster_std=0.60, random_state=0)
plt.scatter(X[:, 0], X[:, 1], s=50)

from sklearn.cluster import KMeans
kmeans = KMeans(n_clusters=4)
kmeans.fit(X)
y_kmeans = kmeans.predict(X)

plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50, cmap='viridis')
#centers = kmeans.cluster_centers_plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)


# EM GaussianMixture Method

from sklearn.mixture import GaussianMixture
gmm = GaussianMixture(n_components=4)
gmm.fit(X)
labels = gmm.predict(X)
plt.scatter(X[:, 0], X[:, 1], c=labels, s=40, cmap='viridis')
