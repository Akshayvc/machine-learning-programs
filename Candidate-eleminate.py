import pandas as pd #helps in accessing of data

#reading of the data sep->separates by ,  and header is the heading
data=pd.read_csv('pd1.csv', sep=',', header=None)

G = []
S = ['?','?','?','?','?','?']
datalen = len(data)
X = data.values[:, 0:6]
ex = data.values[0,:]
if(ex[-1] == 'Yes'):
    S = X[0,:]
else:
    print('First Exmaple is not positive')

#generalization function where Exin->example in ,S_in->specific in
def Generalize(S_in, Exin):
    features=len(Exin)       #length of the rows
    for i in range(features):
        if(S_in[i]!= Exin[i]):   #replaces with ? if both values are different
            S_in[i]='?'
        else:
            S_in[i]=Exin[i]     #otherwise Exin value is placed in S_in
    return(S_in)

def Specialize(G,S,Ex, NegCount):
    if(NegCount == 1):
        G = ConstructG(G,S,Ex)
    else:
        G = Pruning(G,Ex)
    return(G)

def ConstructG(G,S,ex):
    if(S[0] != ex[0] and S[0] != '?'):
        G.append([S[0], '?', '?', '?', '?' , '?'])
    if(S[1] != ex[1] and S[1] != '?'):
        G.append(['?', S[1], '?', '?', '?', '?' ])
    if(S[2] != ex[2] and S[2] != '?'):
        G.append(['?', '?', S[2], '?', '?', '?' ])
    if(S[3] != ex[3] and S[3] != '?'):
        G.append(['?', '?', '?', S[3], '?' , '?'])
    if(S[4] != ex[4] and S[4] != '?'):
        G.append(['?', '?', '?', '?', S[4], '?'])
    if(S[5] != ex[5] and S[5] != '?'):
        G.append(['?', '?', '?', '?', '?', S[5] ])
        return(G)

def Pruning(G, Ex):
    ind = []
    features = len(Ex)
    for i in range(len(G)):
        for k in range(features):
            if G[i][k] == ex[k]:
                ind.append(i)
    Prune_G = [G[i] for i in ind]
    return(Prune_G)
        
datalen=len(data)        
NegCount = 0        

for i in range(datalen-1):
    ex=data.values[i+1,:]
    if(ex[-1]=='Yes'):
        S=Generalize(S,X[i+1,:]) #calls the generalize function
        if(NegCount > 0):
            G = Specialize(G,S,X[i+1,:], NegCount=0)
    else:
        NegCount = NegCount + 1  #if it gets any negative value, specific increments
        G = Specialize(G, S, X[i+1,:], NegCount)
        #print(G)
                
print("Specific Hypothesis =",S)        
print("General Hypothesis = ",G)  