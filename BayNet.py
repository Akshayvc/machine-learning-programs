"""
7. 7.	Write a program to construct aBayesian network considering medical data. 
Use this model to demonstrate the diagnosis of heart patients using standard 
Heart Disease Data Set. You can use Java/Python ML library classes/API.
"""

import numpy as np
from urllib.request import urlopen 
import urllib 
import matplotlib.pyplot as plt # Visuals 
import seaborn as sns 
import sklearn as skl 
import pandas as pd
from pgmpy.models import BayesianModel 
from pgmpy.estimators import MaximumLikelihoodEstimator, BayesianEstimator 
from pgmpy.inference import VariableElimination

Cleveland_data_URL = 'http://archive.ics.uci.edu/ml/machine-learning-databases/heart-disease/processed.hungarian.data'

names = ['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal', 'heartdisease'] 
heartDisease = pd.read_csv(urlopen(Cleveland_data_URL), names = names)
heartDisease.head() 
del heartDisease['ca'] 
del heartDisease['slope'] 
del heartDisease['thal'] 
del heartDisease['oldpeak'] 
heartDisease = heartDisease.replace('?', np.nan) 
heartDisease.dtypes 
heartDisease.columns

model = BayesianModel([('age', 'trestbps'), ('age', 'fbs'), ('sex', 'trestbps'), ('sex', 'trestbps'), ('exang', 'trestbps'),('trestbps','heartdisease'),('fbs','heartdisease'), ('heartdisease','restecg'),('heartdisease','thalach'),('heartdisease','chol')]) # Learing CPDs using Maximum Likelihood Estimators 

model.fit(heartDisease, estimator=MaximumLikelihoodEstimator) 

print(model.get_cpds('age'))
print(model.get_cpds('chol')) 
print(model.get_cpds('sex')) 
model.get_independencies()


HeartDisease_infer = VariableElimination(model)
q = HeartDisease_infer.query(variables=['heartdisease'], evidence={'age': 32})
print(q['heartdisease'])